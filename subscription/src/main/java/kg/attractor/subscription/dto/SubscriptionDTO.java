package kg.attractor.subscription.dto;

import kg.attractor.subscription.model.Event;
import kg.attractor.subscription.model.Subscription;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubscriptionDTO {
    @Builder.Default
    private  String id = null;
    private String eventId;
    private String email;
    private LocalDate registrationTime;

    public static SubscriptionDTO from(Subscription subscription){

        return  builder()
                .id(subscription.getId())
                .eventId(subscription.getEventId().getId())
                .email(subscription.getEmail())
                .registrationTime(subscription.getRegistrationTime())
                .build();
    }

}
