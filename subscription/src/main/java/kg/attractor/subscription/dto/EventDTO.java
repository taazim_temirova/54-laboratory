package kg.attractor.subscription.dto;


import kg.attractor.subscription.model.Event;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class EventDTO {

    private String id;
    private String name;
    private  String description;
    private LocalDate time;


    public static EventDTO from(Event event){
        return builder()
                .id(event.getId())
                .name(event.getName())
                .description(event.getDescription())
                .time(event.getTime())
                .build();
    }
}
