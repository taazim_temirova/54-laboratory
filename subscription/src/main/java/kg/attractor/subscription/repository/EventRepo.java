package kg.attractor.subscription.repository;

import kg.attractor.subscription.model.Event;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EventRepo extends PagingAndSortingRepository<Event, String> {
    @Query("{ 'title' : { $regex : '?0', $options : 'i' } } ")
    List<Event> findByTitle(String title);

}
