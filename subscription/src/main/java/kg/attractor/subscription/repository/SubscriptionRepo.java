package kg.attractor.subscription.repository;

import kg.attractor.subscription.model.Subscription;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubscriptionRepo extends PagingAndSortingRepository<Subscription, String> {

    Slice<Subscription> findByEmail(String email, Pageable pageable);
    Slice<Subscription> findById(String id, Pageable pageable);
    Subscription findByEmailAndEventId(String email, String eventId);

    Slice<Subscription> findAllByEmail(String userEmail, Pageable pageable);

    boolean existsByIdAndEmail(String id, String email);

    void deleteByIdAndEmail(String id, String email);

}
