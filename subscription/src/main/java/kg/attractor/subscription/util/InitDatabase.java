//package kg.attractor.subscription.util;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import kg.attractor.subscription.model.Event;
//import kg.attractor.subscription.model.Subscription;
//import kg.attractor.subscription.repository.EventRepo;
//import kg.attractor.subscription.repository.SubscriptionRepo;
//import kg.attractor.subscription.repository.UserRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//import java.util.stream.Stream;
//
//import static java.util.stream.Collectors.*;
//
//@Configuration
//public class InitDatabase {
//
//    private static final Random r = new Random();
//
//    @Bean
//    CommandLineRunner init(UserRepository userRepo, EventRepo eventRepo, SubscriptionRepo subscriptionRepo) {
//        return (args) -> {
//            subscriptionRepo.deleteAll();
//            userRepo.deleteAll();
//            eventRepo.deleteAll();
//
//            var demoUser = Subscription.random();
//            demoUser.setEmail("demo@demo");
//            subscriptionRepo.save(demoUser);
//
//            List<Event> events = readMovies("events.json");
//
//            List<Subscription> users = Stream.generate(Subscription::random)
//                    .limit(10)
//                    .collect(toList());
//            subscriptionRepo.saveAll(users);
//
//            List<Subscription> subscriptions = new ArrayList<>();
//
//            users.forEach(user -> {
//                selectRandomEvents(events, r.nextInt(3)+1).stream()
//                        .map(event -> Subscription.random(event))
//                        .peek(subscriptions::add)
//                        .forEach(subscriptionRepo::save);
//            });
//
//            subscriptions.stream()
//                    .collect(groupingBy(Subscription::getEventId, averagingDouble(Subscription::getSubscribe)))
//            .forEach(Event::setSubscribers);
//
//            eventRepo.saveAll(events);
//
//            var ac = eventRepo.findByTitle("BLOOM");
//            System.out.println("done");
//        };
//    }
//
//    private List<Event> selectRandomEvents(List<Event> events, int amountOfEvents) {
//        return Stream.generate(() -> pickRandom(events))
//                .distinct()
//                .limit(amountOfEvents)
//                .collect(toList());
//    }
//
//    private static Event pickRandom(List<Event> events) {
//        return events.get(r.nextInt(events.size()));
//    }
//
//    private static List<Event> readMovies(String fileName) {
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            var data = Files.readString(Paths.get(fileName));
//            var listType = new TypeReference<List<Event>>(){};
//            return mapper.readValue(data, listType);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return List.of();
//    }
//}
