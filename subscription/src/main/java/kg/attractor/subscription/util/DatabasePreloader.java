package kg.attractor.subscription.util;



import kg.attractor.subscription.model.Event;
import kg.attractor.subscription.repository.EventRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;


@Configuration
public class DatabasePreloader {


    @Bean
    CommandLineRunner generateGibberish(EventRepo eventRepository) {
        return args -> {
            eventRepository.deleteAll();
            var events = Stream.generate(Event::random).limit(20).collect(toList());
            eventRepository.saveAll(events);
            eventRepository.findAll().forEach(e -> System.out.println(e));
        };
    }
}