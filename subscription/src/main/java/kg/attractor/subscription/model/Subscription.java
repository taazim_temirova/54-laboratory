package kg.attractor.subscription.model;

import kg.attractor.subscription.util.Generator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Document(collection="subscriptions")
public class Subscription {
    @Id
    @Builder.Default

    private  String id = UUID.randomUUID().toString();
    @DBRef
    private  Event eventId;
    private String email;
    private LocalDate registrationTime;

    public static Subscription random( Event event, String email, LocalDate registrationTime) {
        return builder()
                .eventId(event)
                .email(Generator.makeEmail())
                .registrationTime(registrationTime)
                .build();
    }

}
