package kg.attractor.subscription.model;

import kg.attractor.subscription.util.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Document(collection="events")
public class Event {
    private static Random r = new Random();

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Indexed
    @Builder.Default
    private String name;
    private  String description;
    private LocalDate time;

    public static Event random(){
        int t = r.nextInt(2);
        LocalDate n = LocalDate.now();
        n = (t == 1) ? n.plusDays(r.nextInt(5)) : n.minusDays(r.nextInt(5));
        return builder()
                .time(n)
                .description(Generator.makeDescription())
                .name(Generator.makeName())
                .build();

    }
}
