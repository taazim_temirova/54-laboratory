package kg.attractor.subscription.controller;


import kg.attractor.subscription.annotations.ApiPageable;
import kg.attractor.subscription.dto.EventDTO;
import kg.attractor.subscription.dto.SubscriptionDTO;
import kg.attractor.subscription.service.SubscriptionService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/subscriptions")
public class SubscriptionController {
    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @ApiPageable
    @GetMapping
    public Slice<SubscriptionDTO> getAllSubscribes(@ApiIgnore Pageable pageable) {
        return subscriptionService.findAllSubscribes(pageable);
    }
    @ApiPageable
    @GetMapping("/{email}")
    public List<EventDTO> getUserSubscribes(@PathVariable("email") String userEmail, @ApiIgnore Pageable pageable) {
        return subscriptionService.findAllUserEvents(userEmail, pageable);
    }

    @DeleteMapping("/{id}/{userEmail}")
    public String deleteSubscribe(@PathVariable("id") String subscribeId, @PathVariable("email") String userEmail) {
        return subscriptionService.deleteSubscribe(subscribeId, userEmail);
    }
}
