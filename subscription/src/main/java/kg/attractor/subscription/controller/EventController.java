package kg.attractor.subscription.controller;

import kg.attractor.subscription.annotations.ApiPageable;
import kg.attractor.subscription.dto.EventDTO;
import kg.attractor.subscription.dto.SubscriptionDTO;
import kg.attractor.subscription.model.Event;
import kg.attractor.subscription.service.EventService;
import kg.attractor.subscription.service.SubscriptionService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/events")
public class EventController {
    private final EventService eventService;
    private  final SubscriptionService subscriptionService;

    public EventController(SubscriptionService subscriptionService, EventService eventService) {
        this.subscriptionService = subscriptionService;
        this.eventService = eventService;
    }

    @ApiPageable
    @GetMapping
    public Slice<EventDTO> findMovies(@ApiIgnore Pageable pageable) {
        return eventService.findAllEvents(pageable);
    }


    @GetMapping("/{eventId}")
    public EventDTO getMovie(@PathVariable String eventId) {
        return eventService.findOne(eventId);
    }

    @PutMapping("/{eventId}/{email}")
    public String subscribeToEvent(@PathVariable("eventId") String eventId,
                                   @PathVariable("email") String userEmail) {
        return eventService.subscribeToEvent(eventId, userEmail);
    }
}
