package kg.attractor.subscription.service;

import kg.attractor.subscription.dto.EventDTO;
import kg.attractor.subscription.dto.SubscriptionDTO;
import kg.attractor.subscription.exception.ResourceNotFoundException;
import kg.attractor.subscription.model.Event;
import kg.attractor.subscription.repository.EventRepo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class EventService {
    private final EventRepo eventRepo;
    private final SubscriptionService subscriptionService;

    public EventService(EventRepo eventRepository, SubscriptionService subscribeService) {
        this.eventRepo= eventRepository;
        this.subscriptionService = subscribeService;
    }
    public Slice<EventDTO> findAllEvents(Pageable pageable) {
        var slice = eventRepo.findAll(pageable);
        return slice.map(EventDTO::from);
    }

    public boolean isSubscribeable(String eventId) {
        var event = eventRepo.findById(eventId).get();
        boolean e = event.getTime().isAfter(LocalDate.now());
        return e;
    }


    public EventDTO findOne(String eventId) {
        var movie = eventRepo.findById(eventId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find movie with the ID: " + eventId));
        return EventDTO.from(movie);
    }


    public String subscribeToEvent(String eventId, String userEmail) {
        String msg = "";
        if(isSubscribeable(eventId)) {
            Event event = eventRepo.findById(eventId).get();
            msg = subscriptionService.addSubscribe(event, userEmail, LocalDate.now());
        } else {
            msg = "you are late event passed";
        }
        return msg;
    }


}
