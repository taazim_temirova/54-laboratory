package kg.attractor.subscription.service;

import kg.attractor.subscription.dto.EventDTO;
import kg.attractor.subscription.dto.SubscriptionDTO;
import kg.attractor.subscription.model.Event;
import kg.attractor.subscription.model.Subscription;
import kg.attractor.subscription.repository.SubscriptionRepo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class SubscriptionService {
    private  final SubscriptionRepo subscriptionRepo;

    public SubscriptionService(SubscriptionRepo subscriptionRepo) {
        this.subscriptionRepo = subscriptionRepo;
    }

    public String addSubscribe(Event event, String userEmail, LocalDate date){
        String msg = "";
        Subscription s = subscriptionRepo.findByEmailAndEventId(userEmail, event.getId());
        if(s != null) {
            msg = "You have already subscribed to: " + s.getEventId().getName() + " Subscribe ID: " + s.getId();
        } else {
            s = Subscription.random(event, userEmail, date);
            msg = "You are subscribed to: " + s.getEventId().getName() + " Subscribe ID: " + s.getId();
        }
        subscriptionRepo.save(s);
        return msg;

    }

    public Slice<SubscriptionDTO> findAllSubscribes(Pageable pageable) {
        var slice = subscriptionRepo.findAll(pageable);
        return slice.map(SubscriptionDTO::from);
    }

    public List<EventDTO> findAllUserEvents(String userEmail, Pageable pageable) {
        var slice = subscriptionRepo.findAllByEmail(userEmail, pageable);
        List<EventDTO> userEvents = new ArrayList<>();
        slice.forEach(s -> userEvents.add(EventDTO.from(s.getEventId())));
        return userEvents;
    }

    public Slice<SubscriptionDTO> findSubscribe(String id, Pageable pageable) {
        var slice = subscriptionRepo.findById(id, pageable);
        return slice.map(SubscriptionDTO::from);
    }


    public String deleteSubscribe(String subscribeId, String userEmail) {
        String msg = "";
        if(subscriptionRepo.existsByIdAndEmail(subscribeId, userEmail)) {
            subscriptionRepo.deleteByIdAndEmail(subscribeId, userEmail);
            msg = subscribeId + " unsubscribed by " + userEmail;
        } else {
            msg = subscribeId + " not found";
        }
        return msg;
    }
}
